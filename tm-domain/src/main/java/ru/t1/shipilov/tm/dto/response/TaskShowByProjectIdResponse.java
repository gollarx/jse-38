package ru.t1.shipilov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskShowByProjectIdResponse extends AbstractResponse {

    @NotNull
    private List<Task> tasks;

    public TaskShowByProjectIdResponse(@NotNull final List<Task> tasks) {
        this.tasks = tasks;
    }

}
