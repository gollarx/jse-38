package ru.t1.shipilov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.shipilov.tm.api.repository.IProjectRepository;
import ru.t1.shipilov.tm.api.service.IConnectionService;
import ru.t1.shipilov.tm.api.service.IPropertyService;
import ru.t1.shipilov.tm.exception.entity.EntityNotFoundException;
import ru.t1.shipilov.tm.marker.UnitCategory;
import ru.t1.shipilov.tm.model.Project;
import ru.t1.shipilov.tm.service.ConnectionService;
import ru.t1.shipilov.tm.service.PropertyService;

import java.sql.Connection;
import java.util.*;

import static ru.t1.shipilov.tm.constant.ProjectTestData.*;


@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private IProjectRepository repository;

    @NotNull
    private List<Project> projectList;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private Connection connection;

    @BeforeClass
    public static void changeSchema() {
        System.setProperty("database.schema", "tm-tst");
    }

    @Before
    public void init() throws Exception {
        repository = new ProjectRepository();
        connection = connectionService.getConnection();
        connection.setAutoCommit(true);
        repository.setRepositoryConnection(connection);
        projectList = new ArrayList<>();
        for (int i = 1; i <= INIT_COUNT_PROJECTS; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project_" + i);
            project.setDescription("Description_" + i);
            repository.add(USER_ID_1, project);
            project.setUserId(USER_ID_1);
            projectList.add(project);
        }
        for (int i = 1; i <= INIT_COUNT_PROJECTS; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project_" + i);
            project.setDescription("Description_" + i);
            repository.add(USER_ID_2, project);
            project.setUserId(USER_ID_2);
            projectList.add(project);
        }
    }

    @After
    public void closeConnection() throws Exception {
        repository.clear();
        connection.close();
    }

    @Test
    public void testAddProjectPositive() throws Exception {
        Project project = new Project();
        project.setName("ProjectAddTest");
        project.setDescription("ProjectAddTest desc");
        Assert.assertNull(repository.add(NULLABLE_USER_ID, project));
        Assert.assertNotNull(repository.add(USER_ID_1, project));
    }

    @Test
    public void testClear() throws Exception {
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_1));
        repository.clear(USER_ID_1);
        Assert.assertEquals(0, repository.getSize(USER_ID_1));

        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_2));
        repository.clear(USER_ID_2);
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testClearAll() throws Exception {
        Assert.assertEquals(10, repository.getSize());
        repository.clear();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testFindById() throws Exception {
        Assert.assertNull(repository.findOneById(USER_ID_1, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(USER_ID_2, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString()));
        for (@NotNull final Project project : projectList) {
            final Project foundProjectWOUser = repository.findOneById(project.getId());
            final Project foundProject = repository.findOneById(project.getUserId(), project.getId());
            Assert.assertNotNull(foundProjectWOUser);
            Assert.assertNotNull(foundProject);
            Assert.assertEquals(project.getId(), foundProjectWOUser.getId());
            Assert.assertEquals(project.getId(), foundProject.getId());
        }
    }

    @Test
    public void testExistsById() throws Exception {
        Assert.assertFalse(repository.existsById(USER_ID_1, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(USER_ID_2, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        for (@NotNull final Project project : projectList) {
            Assert.assertTrue(repository.existsById(project.getId()));
            Assert.assertTrue(repository.existsById(project.getUserId(), project.getId()));
        }
    }

    @Test
    public void testFindByIndex() throws Exception {
        Assert.assertNull(repository.findOneByIndex(USER_ID_1, 9999));
        Assert.assertNull(repository.findOneByIndex(USER_ID_2, 9999));
        Assert.assertNull(repository.findOneByIndex(9999));
        for (int i = 0; i < INIT_COUNT_PROJECTS * 2; i++) {
            final Project foundProjectWOUser = repository.findOneByIndex(projectList.indexOf(projectList.get(i)) + 1);
            Assert.assertNotNull(foundProjectWOUser);
            Assert.assertEquals(projectList.get(i).getId(), foundProjectWOUser.getId());
        }
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            final Project foundProject = repository.findOneByIndex(USER_ID_1, i + 1);
            Assert.assertNotNull(foundProject);
            Assert.assertEquals(projectList.get(i).getId(), foundProject.getId());
        }
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            final Project foundProject = repository.findOneByIndex(USER_ID_2, i + 1);
            Assert.assertNotNull(foundProject);
            Assert.assertEquals(projectList.get(i + 5).getId(), foundProject.getId());
        }
    }

    @Test
    public void testFindAll() throws Exception {
        List<Project> projects = repository.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectList.size(), projects.size());
        for (int i = 0; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertEquals(projects.get(i).getId(), projectList.get(i).getId());
        }
        projects = repository.findAll(USER_ID_1);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Assert.assertEquals(projects.get(i).getId(), projectList.get(i).getId());
        }
        projects = repository.findAll(USER_ID_2);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertEquals(projects.get(i - 5).getId(), projectList.get(i).getId());
        }
    }

    @Test
    public void testFindAllComparator() throws Exception {
        List<Project> projects = repository.findAll(PROJECT_COMPARATOR);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectList.size(), projects.size());
        for (final Project project : projectList) {
            Assert.assertNotNull(
                    projects.stream()
                            .filter(m -> project.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null));
        }
        projects = repository.findAll(USER_ID_1, PROJECT_COMPARATOR);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        projects = repository.findAll(USER_ID_2, PROJECT_COMPARATOR);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_2)) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveByIdWOUserNegative() throws Exception {
        repository.removeById(UUID.randomUUID().toString());
    }

    @Test
    public void testRemoveByIdWOUserPositive() throws Exception {
        Assert.assertEquals(INIT_COUNT_PROJECTS * 2, repository.getSize());
        for (int i = 0; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertNotNull(repository.removeById(projectList.get(i).getId()));
            Assert.assertNull(repository.findOneById(projectList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemoveById() throws Exception {
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Assert.assertNull(repository.removeById(USER_ID_1, UUID.randomUUID().toString()));
            Assert.assertNotNull(repository.removeById(USER_ID_1, projectList.get(i).getId()));
            Assert.assertNull(repository.findOneById(USER_ID_1, projectList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertNull(repository.removeById(USER_ID_2, UUID.randomUUID().toString()));
            Assert.assertNotNull(repository.removeById(USER_ID_2, projectList.get(i).getId()));
            Assert.assertNull(repository.findOneById(USER_ID_2, projectList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveByIndexWOUserNegative() throws Exception {
        repository.removeByIndex(9999);
    }

    @Test
    public void testRemoveByIndexWOUserPositive() throws Exception {
        Assert.assertEquals(INIT_COUNT_PROJECTS * 2, repository.getSize());
        for (int i = 0; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertNotNull(repository.removeByIndex(1));
            Assert.assertEquals(INIT_COUNT_PROJECTS * 2 - i - 1, repository.getSize());
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemoveByIndex() throws Exception {
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Assert.assertNull(repository.removeByIndex(USER_ID_1, 9999));
            Assert.assertNotNull(repository.removeByIndex(USER_ID_1, 1));
            Assert.assertEquals(INIT_COUNT_PROJECTS - i - 1, repository.getSize(USER_ID_1));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_2));
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Assert.assertNull(repository.removeByIndex(USER_ID_2, 9999));
            Assert.assertNotNull(repository.removeByIndex(USER_ID_2, 1));
            Assert.assertEquals(INIT_COUNT_PROJECTS - i - 1, repository.getSize(USER_ID_2));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveWOUserPositive() throws Exception {
        Assert.assertEquals(INIT_COUNT_PROJECTS * 2, repository.getSize());
        for (final Project project : projectList) {
            Assert.assertNotNull(repository.remove(project));
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemove() throws Exception {
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Assert.assertNotNull(repository.remove(USER_ID_1, projectList.get(i)));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertNotNull(repository.remove(USER_ID_2, projectList.get(i)));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testSet() throws Exception {
        Assert.assertNotNull(repository.set(Arrays.asList(new Project(), new Project(), new Project())));
        Assert.assertEquals(3, repository.getSize());
    }

}
