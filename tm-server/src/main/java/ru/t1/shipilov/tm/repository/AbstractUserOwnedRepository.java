package ru.t1.shipilov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.api.repository.IUserOwnedRepository;
import ru.t1.shipilov.tm.enumerated.DBColumns;
import ru.t1.shipilov.tm.model.AbstractUserOwnedModel;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NotNull
    private String USER_ID_COLUMN = DBColumns.USER_ID_COLUMN.getColumnName();

    @Override
    public void clear(@NotNull final String userId) throws Exception {
        @NotNull final String query = String.format("DELETE FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) throws Exception {
        return findOneById(userId, id) != null;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String query = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String query = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        @NotNull final String order = String.format(" ORDER BY %s", getSortType(comparator));
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query + order)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        @NotNull final String query = String.format("SELECT * FROM %s WHERE id = ? AND %s = ? LIMIT 1", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) throws Exception {
        @NotNull final String query = String.format("SELECT * FROM %s WHERE %s = ? LIMIT ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setInt(2, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            for (int i = 0; i < index - 1; i++) {
                if (!resultSet.next()) return null;
            }
            return fetch(resultSet);
        }
    }

    @Override
    public int getSize(@NotNull final String userId) throws Exception {
        @NotNull final String query = String.format("SELECT COUNT(ID) FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt("COUNT");
        }
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || id == null) return null;
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final String userId, @NotNull final Integer index) throws Exception {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @NotNull final M model) throws Exception {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) throws Exception {
        @NotNull final String query = String.format("DELETE FROM %s WHERE ID = ? AND %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, model.getId());
            statement.setString(2, userId);
            statement.executeUpdate();
        }
        return model;
    }

}
