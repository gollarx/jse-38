package ru.t1.shipilov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.api.repository.ISessionRepository;
import ru.t1.shipilov.tm.api.service.IConnectionService;
import ru.t1.shipilov.tm.api.service.ISessionService;
import ru.t1.shipilov.tm.model.Session;

import java.sql.Connection;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull ISessionRepository repository, @NotNull IConnectionService connectionService) {
        super(repository, connectionService);
    }

    @Override
    public void setRepositoryConnection(Connection connection) {
        repository.setRepositoryConnection(connection);
    }

}
